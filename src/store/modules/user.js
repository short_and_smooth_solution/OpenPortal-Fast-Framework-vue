export default {
  namespaced: true,
  state: {
    id: 0,
    name: '',
    deptId: 0
  },
  mutations: {
    updateId (state, id) {
      state.id = id
    },
    updateName (state, name) {
      state.name = name
    },
    updateDeptId (state, deptId) {
      state.deptId = deptId
    }
  }
}
